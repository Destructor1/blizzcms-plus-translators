<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Dashboard';
$lang['admin_nav_settings'] = 'Settings';
$lang['admin_nav_website_settings'] = 'Website Settings';
$lang['admin_nav_manage_modules'] = 'Manage Modules';
$lang['admin_nav_manage_realms'] = 'Manage Realms';
$lang['admin_nav_manage_slides'] = 'Manage Slides';
$lang['admin_nav_users'] = 'Users';
$lang['admin_nav_users_list'] = 'Users List';
$lang['admin_nav_chars_list'] = 'Characters List';
$lang['admin_nav_website'] = 'Website';
$lang['admin_nav_news'] = 'News';
$lang['admin_nav_changelogs'] = 'Changelogs';
$lang['admin_nav_pages'] = 'Pages';
$lang['admin_nav_faq'] = 'Faq';
$lang['admin_nav_donations'] = 'Donations';
$lang['admin_nav_topsites'] = 'Topsites';
$lang['admin_nav_store'] = 'Store';
$lang['admin_nav_manage_groups'] = 'Manage Groups';
$lang['admin_nav_manage_items'] = 'Manage Items';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_categories'] = 'Manage Categories';
$lang['admin_nav_manege_forums'] = 'Manage Forums';

/*Button Lang*/
$lang['button_unban'] = 'Unban';
$lang['button_ban'] = 'Ban';
$lang['button_re_grant_account'] = 'Remove GM';
$lang['button_grant_account'] = 'Grant GM';
$lang['button_change_level'] = 'Change Level';
$lang['button_disable'] = 'Disable';
$lang['button_enable'] = 'Enable';

/*Card Title Lang*/
$lang['card_title_user_manage'] = 'Manage user';
$lang['card_title_unban_account'] = 'Unban Account';
$lang['card_title_ban_account'] = 'Ban Account';
$lang['card_title_rank_account'] = 'GM Rank';
$lang['card_title_general_info'] = 'General information';
$lang['card_title_donate_history'] = 'Donate History';
$lang['card_title_annotations'] = 'Annotations';
$lang['card_title_char_manage'] = 'Manage character';
$lang['card_title_change_level'] = 'Change Level';
$lang['card_title_rename'] = 'Rename';
$lang['card_title_unban_char'] = 'Unban Character';
$lang['card_title_ban_char'] = 'Ban Character';
$lang['card_title_customize'] = 'Customize Character';
$lang['card_title_change_race'] = 'Change Race';
$lang['card_title_change_faction'] = 'Change Faction';
$lang['card_title_changelogs_list'] = 'List of Changelogs';
$lang['card_title_pages_list'] = 'List of Pages';
$lang['card_title_news_list'] = 'News List';
$lang['card_title_edit_news'] = 'Edit News';
$lang['card_title_edit_pages'] = 'Edit Pages';
$lang['card_title_edit_changelogs'] = 'Edit Changelogs';
$lang['card_title_edit_item'] = 'Edit Item';
$lang['card_title_edit_group'] = 'Edit Group';
$lang['card_title_faq_list'] = 'Faq List';
$lang['card_title_edit_topsite'] = 'Edit Topsite';
$lang['card_title_edit_forum'] = 'Edit Forum';

/*Table header Lang*/
$lang['table_header_race'] = 'Race';
$lang['table_header_class'] = 'Class';
$lang['table_header_level'] = 'Level';
$lang['table_header_money'] = 'Money';
$lang['table_header_own'] = 'Own';
$lang['table_header_action'] = 'Action';
$lang['table_header_realm_id'] = 'Realm ID';
$lang['table_header_realm_name'] = 'Realm Name';
$lang['table_header_realm_char_database'] = 'Character Database Name';
$lang['table_header_tax'] = 'Tax';
$lang['table_header_points'] = 'Points';
$lang['table_header_type'] = 'Type';
$lang['table_header_module'] = 'Module';

/*Input Placeholder Lang*/
$lang['placeholder_create_changelog'] = 'Create a new Changelog';
$lang['placeholder_changelog_title'] = 'Title of the Changelog';
$lang['placeholder_create_pages'] = 'Create a new Page';
$lang['placeholder_create_news'] = 'Create a new News';
$lang['placeholder_create_donation'] = 'Create Donation';
$lang['placeholder_donation_title'] = 'Title of the donation';
$lang['placeholder_news_title'] = 'Title of the news';
$lang['placeholder_upload_file'] = 'File upload';
$lang['placeholder_create_category'] = 'Create Category';
$lang['placeholder_create_forums'] = 'Create Forums';
$lang['placeholder_category_title'] = 'Title of the Category';
$lang['placeholder_forum_title'] = 'Title of the Forum';
$lang['placeholder_forum_description'] = 'Enter a brief description of the forum';
$lang['placeholder_forum_icon_name'] = 'Icon Name';
$lang['placeholder_category'] = 'Category';
$lang['placeholder_create_item'] = 'Create a item';
$lang['placeholder_store_item_name'] = 'Product Name';
$lang['placeholder_store_item_id'] = 'Item Id';
$lang['placeholder_store_image_name'] = 'Image File Name';
$lang['placeholder_create_group'] = 'Create Group';
$lang['placeholder_group_title'] = 'Title of the Group';
$lang['placeholder_create_faq'] = 'Create faq';
$lang['placeholder_faq_title'] = 'Title of the faq';
$lang['placeholder_create_topsite'] = 'Create a new Topsite';
$lang['placeholder_reason'] = 'Reason';
$lang['placeholder_gmlevel'] = 'Gm Level';
$lang['placeholder_forum_icon'] = 'foldername/image.jpg or foldername/image.png';

/*Status Lang*/
$lang['status_is_online'] = 'Player is Online, please disconnect';
$lang['status_name_exist'] = 'This name already exist';
$lang['status_donate_complete'] = 'Complete';
$lang['status_donate_cancell'] = 'Cancelled';

/*Options Lang*/
$lang['option_yes'] = 'Yes';
$lang['option_no'] = 'No';
$lang['option_everyone'] = 'Everyone';
$lang['option_staff'] = 'STAFF';
$lang['option_all'] = 'STAFF - Everyone';
$lang['option_item'] = 'Item';

/*Count Lang*/
$lang['count_accounts_created'] = 'Accounts created';
$lang['count_accounts_banned'] = 'Banned accounts';
$lang['count_news_created'] = 'News Created';
$lang['count_changelogs_created'] = 'Changelogs Created';
$lang['total_accounts_registered'] = 'Total accounts registered.';
$lang['total_accounts_banned'] = 'Total accounts banned.';
$lang['total_news_writed'] = 'Total news writed.';
$lang['total_changelogs_writed'] = 'Total changelogs writed.';

/*Alert Lang*/
$lang['alert_new_page_url'] = 'Your new Page URL is';
$lang['alert_banned_reason'] = 'Is banned, reason:';

/*Logs Lang*/
$lang['log_new_level'] = 'Receive a new level';
$lang['log_old_level'] = 'Before it was';
$lang['log_new_name'] = 'It has a new name';
$lang['log_old_name'] = 'Before it was';
$lang['log_unbanned'] = 'Unbanned';
$lang['log_customization'] = 'Get a customization';
$lang['log_change_race'] = 'Get a Race Change';
$lang['log_change_faction'] = 'Get a Faction Change';
$lang['log_banned'] = 'Was banned';
$lang['log_gm_assigned'] = 'Received GM rank';
$lang['log_gm_removed'] = 'The GM rank was removed';
