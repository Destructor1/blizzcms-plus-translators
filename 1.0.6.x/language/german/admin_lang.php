<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Dashboard';
$lang['admin_nav_system'] = 'System';
$lang['admin_nav_manage_settings'] = 'Einstellungen verwalten';
$lang['admin_nav_manage_modules'] = 'Module bearbeiten';
$lang['admin_nav_users'] = 'Nutzer';
$lang['admin_nav_accounts'] = 'Accounts';
$lang['admin_nav_website'] = 'Website';
$lang['admin_nav_menu'] = 'Menü';
$lang['admin_nav_realms'] = 'Realms';
$lang['admin_nav_slides'] = 'Slides';
$lang['admin_nav_news'] = 'Neuigkeiten';
$lang['admin_nav_changelogs'] = 'Änderungsprotokoll';
$lang['admin_nav_pages'] = 'Seiten';
$lang['admin_nav_donate_methods'] = 'Spendensystem';
$lang['admin_nav_topsites'] = 'Topseiten';
$lang['admin_nav_donate_vote_logs'] = 'Spenden/Abstimmungs Protokolle';
$lang['admin_nav_store'] = 'Shop';
$lang['admin_nav_manage_store'] = 'Shop verwalten';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_forum'] = 'Forum verwalten';
$lang['admin_nav_logs'] = 'Protokolle';

/*Sections Lang*/
$lang['section_general_settings'] = 'Allgemeine Einstellungen';
$lang['section_module_settings'] = 'Module Einstellungen';
$lang['section_optional_settings'] = 'Optionale Einstellung';
$lang['section_seo_settings'] = 'SEO Einstellung';
$lang['section_update_cms'] = 'Updates';
$lang['section_check_information'] = 'Informationen überprüfen';
$lang['section_forum_categories'] = 'Forum Kategorien';
$lang['section_forum_elements'] = 'Forum Elemente';
$lang['section_store_categories'] = 'Shop Kategorien';
$lang['section_store_items'] = 'Shop Items';
$lang['section_store_top'] = 'Favorisierte Items im Shop';
$lang['section_logs_dp'] = 'Spende Protokolle';
$lang['section_logs_vp'] = 'Abstimmung Protokolle';

/*Button Lang*/
$lang['button_select'] = 'Auswählen';
$lang['button_update'] = 'Aktualisieren';
$lang['button_unban'] = 'Entsperren';
$lang['button_ban'] = 'Sperren';
$lang['button_remove'] = 'Entfernen';
$lang['button_grant'] = 'Rang speichern';
$lang['button_update_version'] = 'Version aktualisieren';

/*Table header Lang*/
$lang['table_header_subcategory'] = 'Wähle eine Unterkategorie';
$lang['table_header_race'] = 'Rasse';
$lang['table_header_class'] = 'Klasse';
$lang['table_header_level'] = 'Level';
$lang['table_header_money'] = 'Gold';
$lang['table_header_time_played'] = 'gespielte Zeit';
$lang['table_header_actions'] = 'Aktionen';
$lang['table_header_id'] = '#ID';
$lang['table_header_tax'] = 'MwSt';
$lang['table_header_points'] = 'Punkte';
$lang['table_header_type'] = 'Type';
$lang['table_header_module'] = 'Module';
$lang['table_header_payment_id'] = 'Zahlungs ID';
$lang['table_header_hash'] = 'Hash';
$lang['table_header_total'] = 'Total';
$lang['table_header_create_time'] = 'Zeitraum der Erstellung';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Information';
$lang['table_header_value'] = 'Wert';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = 'Konto verwalten';
$lang['placeholder_update_information'] = 'Kontoinformation aktualisieren';
$lang['placeholder_donation_logs'] = 'Spenden Protokolle';
$lang['placeholder_store_logs'] = 'Shop Protokolle';
$lang['placeholder_create_changelog'] = 'Änderungsprotokoll erstellen';
$lang['placeholder_edit_changelog'] = 'Änderungsprotokoll bearbeiten';
$lang['placeholder_create_category'] = 'Kategorie erstellen';
$lang['placeholder_edit_category'] = 'Kategorie bearbeiten';
$lang['placeholder_create_forum'] = 'Forum erstellen';
$lang['placeholder_edit_forum'] = 'Forum bearbeiten';
$lang['placeholder_create_menu'] = 'Menu erstellen';
$lang['placeholder_edit_menu'] = 'Menu bearbeiten';
$lang['placeholder_create_news'] = 'News erstellen';
$lang['placeholder_edit_news'] = 'News bearbeiten';
$lang['placeholder_create_page'] = 'Seite erstellen';
$lang['placeholder_edit_page'] = 'Seite bearbeiten';
$lang['placeholder_create_realm'] = 'Realm erstellen';
$lang['placeholder_edit_realm'] = 'Realm bearbeiten';
$lang['placeholder_create_slide'] = 'Slide erstellen';
$lang['placeholder_edit_slide'] = 'Slide bearbeiten';
$lang['placeholder_create_item'] = 'Item erstellen';
$lang['placeholder_edit_item'] = 'Item bearbeiten';
$lang['placeholder_create_topsite'] = 'Topseite erstellen';
$lang['placeholder_edit_topsite'] = 'Topseite bearbeiten';
$lang['placeholder_create_top'] = 'Favorisiertes Item erstellen';
$lang['placeholder_edit_top'] = 'Favorisiertes Item bearbeiten';

$lang['placeholder_upload_image'] = 'Bild hochladen';
$lang['placeholder_icon_name'] = 'Symbolname';
$lang['placeholder_category'] = 'Kategorie';
$lang['placeholder_name'] = 'Name';
$lang['placeholder_item'] = 'Item';
$lang['placeholder_image_name'] = 'Bildname';
$lang['placeholder_reason'] = 'Grund';
$lang['placeholder_gmlevel'] = 'GM-Level';
$lang['placeholder_url'] = 'Link/URL';
$lang['placeholder_child_menu'] = 'Untermenü';
$lang['placeholder_url_type'] = 'Art des Links';
$lang['placeholder_route'] = 'Route';
$lang['placeholder_hours'] = 'Stunden';
$lang['placeholder_soap_hostname'] = 'Soap Host/IP';
$lang['placeholder_soap_port'] = 'Soap Port';
$lang['placeholder_soap_user'] = 'Soap Benutzer';
$lang['placeholder_soap_password'] = 'Soap Passwort';
$lang['placeholder_db_character'] = 'Charakter';
$lang['placeholder_db_hostname'] = 'Datenbank Host/IP';
$lang['placeholder_db_name'] = 'Datenbank Name';
$lang['placeholder_db_user'] = 'Datenbank Benutzer';
$lang['placeholder_db_password'] = 'Datenbank Passwort';
$lang['placeholder_account_points'] = 'Konto Punkte';
$lang['placeholder_account_ban'] = 'Konto sperren';
$lang['placeholder_account_unban'] = 'Konto entsperren';
$lang['placeholder_account_grant_rank'] = 'GM Rang hinzufügen';
$lang['placeholder_account_remove_rank'] = 'GM Rang entfernen';
$lang['placeholder_command'] = 'Befehl';

/*Config Lang*/
$lang['conf_website_name'] = 'Webseiten Name';
$lang['conf_realmlist'] = 'Realmlist';
$lang['conf_discord_invid'] = 'Discord Server ID';
$lang['conf_timezone'] = 'Zeitzone';
$lang['conf_theme_name'] = 'Design Name';
$lang['conf_maintenance_mode'] = 'Wartungsmodus';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter URL';
$lang['conf_social_youtube'] = 'Youtube URL';
$lang['conf_paypal_currency'] = 'Währung';
$lang['conf_paypal_mode'] = 'Methode';
$lang['conf_paypal_client'] = 'PayPal Client/ID';
$lang['conf_paypal_secretpass'] = 'PayPal Passwort';
$lang['conf_default_description'] = 'Standardbeschreibung';
$lang['conf_admin_gmlvl'] = 'Administrator ab Rang:';
$lang['conf_mod_gmlvl'] = 'Moderator ab Rang:';
$lang['conf_recaptcha_key'] = 'Seitenschlüssel';
$lang['conf_account_activation'] = 'Account Aktivierung';
$lang['conf_smtp_hostname'] = 'SMTP Host/IP';
$lang['conf_smtp_port'] = 'SMTP Port';
$lang['conf_smtp_encryption'] = 'SMTP Verschlüsselung';
$lang['conf_smtp_username'] = 'SMTP Nutzername';
$lang['conf_smtp_password'] = 'SMTP Passwort';
$lang['conf_sender_email'] = 'Email des Versendenden';
$lang['conf_sender_name'] = 'Name des Versendenden';

/*Logs */
$lang['placeholder_logs_dp'] = 'Spenden';
$lang['placeholder_logs_quantity'] = 'Menge';
$lang['placeholder_logs_hash'] = 'Hash';
$lang['placeholder_logs_voteid'] = 'ID';
$lang['placeholder_logs_points'] = 'Punkte';
$lang['placeholder_logs_lasttime'] = 'zuletzt am:';
$lang['placeholder_logs_expiredtime'] = 'Abgelaufene Zeit';

/*Status Lang*/
$lang['status_completed'] = 'Abgeschlossen';
$lang['status_cancelled'] = 'Unterbrochen';

/*Options Lang*/
$lang['option_normal'] = 'Normal';
$lang['option_dropdown'] = 'Dropdown-Liste';
$lang['option_image'] = 'Bild';
$lang['option_video'] = 'Video';
$lang['option_iframe'] = 'Iframe';
$lang['option_enabled'] = 'aktiviert';
$lang['option_disabled'] = 'deaktiviert';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = 'Jeder';
$lang['option_staff'] = 'Team';
$lang['option_all'] = 'Team/Jeder';
$lang['option_rename'] = 'Umbenennen';
$lang['option_customize'] = 'Anpassen';
$lang['option_change_faction'] = 'Fraktion ändern';
$lang['option_change_race'] = 'Klasse ändern';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP/VP';
$lang['option_internal_url'] = 'Interne URL';
$lang['option_external_url'] = 'Externe URL';
$lang['option_on'] = 'An';
$lang['option_off'] = 'Aus';

/*Count Lang*/
$lang['count_accounts_created'] = 'Erstellte Accounts';
$lang['count_accounts_banned'] = 'Gesperrte Accounts';
$lang['count_news_created'] = 'Geschriebene Neuigkeiten';
$lang['count_changelogs_created'] = 'Erstellte Änderungsprotokolle';
$lang['total_accounts_registered'] = 'Accounts sind registriert.';
$lang['total_accounts_banned'] = 'sind gesperrt worden.';
$lang['total_news_writed'] = 'Neuigkeiten wurden verfasst.';
$lang['total_changelogs_writed'] = 'Änderungsprotokolle sind erstellt worden.';

$lang['info_alliance_players'] = 'Allianzspieler';
$lang['info_alliance_playing'] = 'davon Aktiv:';
$lang['info_horde_players'] = 'Hordespieler';
$lang['info_horde_playing'] = 'davon aktiv:';
$lang['info_players_playing'] = 'Aktive Spieler insgesamt:';

/*Alert Lang*/
$lang['alert_smtp_activation'] = 'Wenn Sie diese Option aktivieren, müssen Sie SMTP zum Senden von E-Mails konfigurieren.';
$lang['alert_banned_reason'] = 'Grund für die Sperre:';

/*Logs Lang*/
$lang['log_new_level'] = 'Neues Level:';
$lang['log_old_level'] = 'vorheriges Level';
$lang['log_new_name'] = 'Neuer Name:';
$lang['log_old_name'] = 'vorheriger Name';
$lang['log_unbanned'] = 'Entsperrt';
$lang['log_customization'] = 'Charakteranpassung';
$lang['log_change_race'] = 'Rasse ändern.';
$lang['log_change_faction'] = 'Fraktionswechsel';
$lang['log_banned'] = 'Account war gesperrt';
$lang['log_gm_assigned'] = 'GM-Rang erhalten';
$lang['log_gm_removed'] = 'Der GM-Rang wurde entfernt';

/*CMS Lang*/
$lang['cms_version_currently'] = 'Diese Version läuft derzeit';
$lang['cms_warning_update'] = 'Wenn das CMS aktualisiert wird, kann die Konfiguration abhängig von den vorgenommenen Änderungen auf den Standardwert zurückgesetzt werden.';
$lang['cms_php_version'] = 'PHP Version';
$lang['cms_allow_fopen'] = 'Modul: fopen';
$lang['cms_allow_include'] = 'Modul: include';
$lang['cms_loaded_modules'] = 'Geladene Module/Apache';
$lang['cms_loaded_extensions'] = 'Geladene Erweiterungen/PHP';
