<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Dashboard';
$lang['admin_nav_system'] = 'System';
$lang['admin_nav_manage_settings'] = 'Hantera Inställningar';
$lang['admin_nav_manage_modules'] = 'Hantera Moduler';
$lang['admin_nav_users'] = 'Användare';
$lang['admin_nav_accounts'] = 'Konton';
$lang['admin_nav_website'] = 'Webbplats';
$lang['admin_nav_menu'] = 'Meny';
$lang['admin_nav_realms'] = 'Realms';
$lang['admin_nav_slides'] = 'Slides';
$lang['admin_nav_news'] = 'Nyheter';
$lang['admin_nav_changelogs'] = 'Changelogs';
$lang['admin_nav_pages'] = 'Sidor';
$lang['admin_nav_donate_methods'] = 'Donera Metoder';
$lang['admin_nav_topsites'] = 'Toppsidor';
$lang['admin_nav_donate_vote_logs'] = 'Donera/Rösta Loggar';
$lang['admin_nav_store'] = 'Butik';
$lang['admin_nav_manage_store'] = 'Hantera Butik';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_forum'] = 'Hantera Forum';
$lang['admin_nav_logs'] = 'Loggar System';

/*Sections Lang*/
$lang['section_general_settings'] = 'Allmänna Inställningar';
$lang['section_module_settings'] = 'Modulinställningar';
$lang['section_optional_settings'] = 'Valfri Inställningar';
$lang['section_seo_settings'] = 'SEO-Inställningar';
$lang['section_update_cms'] = 'Uppdatera CMS';
$lang['section_check_information'] = 'Kontrollera Information';
$lang['section_forum_categoryer'] = 'Forumkategorier';
$lang['section_forum_elements'] = 'Forumelement';
$lang['section_store_categoryer'] = 'Lagra Kategorier';
$lang['section_store_items'] = 'Lagra Artiklar';
$lang['section_store_top'] = 'Lagra TOPP-Objekt';
$lang['section_logs_dp'] = 'Donationsloggar';
$lang['section_logs_vp'] = 'Rösta Loggar';

/*Button Lang*/
$lang['button_select'] = 'Välj';
$lang['button_update'] = 'Uppdatera';
$lang['button_unban'] = 'Avlösta';
$lang['button_ban'] = 'Förbud';
$lang['button_remove'] = 'Ta bort';
$lang['button_grant'] = 'Bevilja';
$lang['button_update_version'] = 'Uppdatera till senaste versionen';

/*Table header Lang*/
$lang['table_header_subcategory'] = 'Välj en underkategori';
$lang['table_header_race'] = 'Race';
$lang['table_header_class'] = 'Klass';
$lang['table_header_level'] = 'Nivå';
$lang['table_header_money'] = 'Pengar';
$lang['table_header_time_played'] = 'Tid Spelad';
$lang['table_header_actions'] = 'Åtgärder';
$lang['table_header_id'] = '#ID';
$lang['table_header_tax'] = 'Skatt';
$lang['table_header_points'] = 'Poäng';
$lang['table_header_type'] = 'Typ';
$lang['table_header_module'] = 'Modul';
$lang['table_header_payment_id'] = 'Betalnings-ID';
$lang['table_header_hash'] = 'Hash';
$lang['table_header_total'] = 'Totalt';
$lang['table_header_create_time'] = 'Skapa Tid';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Information';
$lang['table_header_value'] = 'Värde';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = 'Hantera Konto';
$lang['placeholder_update_information'] = 'Uppdatera Kontoinformation';
$lang['placeholder_donation_logs'] = 'Donationsloggar';
$lang['placeholder_store_logs'] = 'Lagra Loggar';
$lang['placeholder_create_changelog'] = 'Skapa Changelog';
$lang['placeholder_edit_changelog'] = 'Redigera Changelog';
$lang['placeholder_create_category'] = 'Skapa Kategori';
$lang['placeholder_edit_category'] = 'Redigera Kategori';
$lang['placeholder_create_forum'] = 'Skapa Forum';
$lang['placeholder_edit_forum'] = 'Redigera Forum';
$lang['placeholder_create_menu'] = 'Skapa Meny';
$lang['placeholder_edit_menu'] = 'Redigera Meny';
$lang['placeholder_create_news'] = 'Skapa Nyheter';
$lang['placeholder_edit_news'] = 'Redigera Nyheter';
$lang['placeholder_create_page'] = 'Skapa Sida';
$lang['placeholder_edit_page'] = 'Redigera Sida';
$lang['placeholder_create_realm'] = 'Skapa Realm';
$lang['placeholder_edit_realm'] = 'Redigera Realm';
$lang['placeholder_create_slide'] = 'Skapa Bild';
$lang['placeholder_edit_slide'] = 'Redigera Bild';
$lang['placeholder_create_item'] = 'Skapa Objekt';
$lang['placeholder_edit_item'] = 'Redigera Objekt';
$lang['placeholder_create_topsite'] = 'Skapa Topsite';
$lang['placeholder_edit_topsite'] = 'Redigera Webbplatsen';
$lang['placeholder_create_top'] = 'Skapa TOPP-Objekt';
$lang['placeholder_edit_top'] = 'Redigera TOPP-Objekt';

$lang['placeholder_upload_image'] = 'Ladda upp bild';
$lang['placeholder_icon_name'] = 'Ikonnamn';
$lang['placeholder_category'] = 'Kategori';
$lang['placeholder_name'] = 'Namn';
$lang['placeholder_item'] = 'Objekt';
$lang['placeholder_image_name'] = 'Bildnamn';
$lang['placeholder_reason'] = 'Anledning';
$lang['placeholder_gmlevel'] = 'GM-Nivå';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_child_menu'] = 'Undermeny';
$lang['placeholder_url_type'] = 'URL-Typ';
$lang['placeholder_route'] = 'Rutt';
$lang['placeholder_hours'] = 'Timmar';
$lang['placeholder_soap_hostname'] = 'SOAP Värdnamn';
$lang['placeholder_soap_port'] = 'SOAP Port';
$lang['placeholder_soap_user'] = 'SOAP Användare';
$lang['placeholder_soap_password'] = 'SOAP Lösenord';
$lang['placeholder_db_character'] = 'Karaktär';
$lang['placeholder_db_hostname'] = 'Databasvärdnamn';
$lang['placeholder_db_name'] = 'Databasnamn';
$lang['placeholder_db_user'] = 'Databasanvändare';
$lang['placeholder_db_password'] = 'Databaslösenord';
$lang['placeholder_account_points'] = 'Kontopoäng';
$lang['placeholder_account_ban'] = 'Förbjuda Konto';
$lang['placeholder_account_unban'] = 'Ta bort konto';
$lang['placeholder_account_grant_rank'] = 'Bevilja GM Nivå';
$lang['placeholder_account_remove_rank'] = 'Ta bort GM Nivå';
$lang['placeholder_command'] = 'Kommando';

/*Config Lang*/
$lang['conf_website_name'] = 'Webbplatsnamn';
$lang['conf_realmlist'] = 'Realmlist';
$lang['conf_discord_invid'] = 'Discordinbjudnings-ID';
$lang['conf_timezone'] = 'Tidszon';
$lang['conf_theme_name'] = 'Temanamn';
$lang['conf_main maintenance_mode'] = 'Underhållsläge';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter-URL';
$lang['conf_social_youtube'] = 'Youtube URL';
$lang['conf_paypal_currency'] = 'PayPal valuta';
$lang['conf_paypal_mode'] = 'PayPal-läge';
$lang['conf_paypal_client'] = 'PayPal-klient-ID';
$lang['conf_paypal_secretpass'] = 'PayPal hemligt lösenord';
$lang['conf_default_description'] = 'Standardbeskrivning';
$lang['conf_admin_gmlvl'] = 'Administratör GM-Nivå';
$lang['conf_mod_gmlvl'] = 'Moderator GM-Nivå';
$lang['conf_recaptcha_key'] = 'reCaptcha Site Key';
$lang['conf_account_activation'] = 'Kontoaktivering';
$lang['conf_smtp_hostname'] = 'SMTP-värdnamn';
$lang['conf_smtp_port'] = 'SMTP-port';
$lang['conf_smtp_encryption'] = 'SMTP-kryptering';
$lang['conf_smtp_username'] = 'SMTP-användarnamn';
$lang['conf_smtp_password'] = 'SMTP-lösenord';
$lang['conf_sender_email'] = 'Avsändar-e-post';
$lang['conf_sender_name'] = 'Avsändarnamn';

/*Logs */
$lang['placeholder_logs_dp'] = 'Donation';
$lang['placeholder_logs_quantity'] = 'Kvantitet';
$lang['placeholder_logs_hash'] = 'Hash';
$lang['placeholder_logs_voteid'] = 'Rösta ID';
$lang['placeholder_logs_points'] = 'Poäng';
$lang['placeholder_logs_lasttime'] = 'Förra gången';
$lang['placeholder_logs_expiredtime'] = 'Utgått tid';

/*Status Lang*/
$lang['status_completed'] = 'Avslutad';
$lang['status_cancelled'] = 'Avbruten';

/*Options Lang*/
$lang['option_normal'] = 'Normal';
$lang['option_dropdown'] = 'Dropdown';
$lang['option_image'] = 'Bild';
$lang['option_video'] = 'Video';
$lang['option_iframe'] = 'Iframe';
$lang['option_enabled'] = 'Aktiverad';
$lang['option_disabled'] = 'Inaktiverad';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = 'Alla';
$lang['option_staff'] = 'PERSONAL';
$lang['option_all'] = 'PERSONAL - Alla';
$lang['option_rename'] = 'Byt namn';
$lang['option_customize'] = 'Anpassa';
$lang['option_change_faction'] = 'Ändra fraktion';
$lang['option_change_race'] = 'Ändra race';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP & VP';
$lang['option_internal_url'] = 'Intern URL';
$lang['option_external_url'] = 'Extern URL';
$lang['option_on'] = 'På';
$lang['option_off'] = 'Av';

/*Count Lang*/
$lang['count_accounts_created'] = 'Konton skapade';
$lang['count_accounts_banned'] = 'Bannade konton';
$lang['count_news_created'] = 'Nyheter skapade';
$lang['count_changelogs_created'] = 'Changelogs skapad';
$lang['total_accounts_registered'] = 'Totala registrerade konton.';
$lang['total_accounts_banned'] = 'Totala konton förbjudna.';
$lang['total_news_writed'] = 'Totalt skrivna nyheter.';
$lang['total_changelogs_writed'] = 'Totala författare skrivna.';

$lang['info_alliance_players'] = 'Alliance-spelare';
$lang['info_alliance_playing'] = 'Allianser spelar på realm';
$lang['info_horde_players'] = 'Horde Players';
$lang['info_horde_playing'] = 'Horder spelar på realm';
$lang['info_players_playing'] = 'Spelare som spelar på realm';

/*Alert Lang*/
$lang['alert_smtp_activation'] = 'Om du aktiverar detta alternativ måste du konfigurera SMTP för att skicka e-post.';
$lang['alert_banned_reason'] = 'Är förbjuden, orsak:';

/*Logs Lang*/
$lang['log_new_level'] = 'Få en ny nivå';
$lang['log_old_level'] = 'Innan det var';
$lang['log_new_name'] = 'Den har ett nytt namn';
$lang['log_old_name'] = 'Innan det var';
$lang['log_unbanned'] = 'Ej förbjuden';
$lang['log_customization'] = 'Få en anpassning';
$lang['log_change_race'] = 'Få en rasändring';
$lang['log_change_faction'] = 'Få en fraktion förändra';
$lang['log_banned'] = 'Bannades';
$lang['log_gm_assigned'] = 'Erhöll GM-Nivå';
$lang['log_gm_removed'] = 'GM-Nivåen togs bort';

/*CMS Lang*/
$lang['cms_version_currently'] = 'Denna version körs för närvarande';
$lang['cms_warning_update'] = 'När cms uppdateras kan konfigurationen återställas till standard beroende på ändringarna i varje version.';
$lang['cms_php_version'] = 'PHP-version';
$lang['cms_allow_fopen'] = 'allow_url_fopen';
$lang['cms_allow_include'] = 'allow_url_include';
$lang['cms_loaded_modules'] = 'Laddade moduler';
$lang['cms_loaded_extensions'] = 'Lastade tillägg';
