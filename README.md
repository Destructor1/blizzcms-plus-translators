# _BlizzCMS Plus Translators_
Repository for translation and multi-language integration in BlizzCMS.

[![Project Status](https://img.shields.io/badge/Status-In_Development-yellow.svg?style=flat-square)](#)
[![Translators Version](https://img.shields.io/badge/Version-0.0.3-green.svg?style=flat-square)](#)

# _Supported language 1.0.6.4.1_

| Translate | Percentage | Contributors | Status 
| :----------- | :---------- | :---------- | :----------
| **EN** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | WoW CMS Team | Updated
| **ES** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | WoW CMS Team | Updated
| **NO** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | [Mr.Angel](https://gitlab.com/vortexed.profesional) | Updated
| **SWE** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | [Mr.Angel](https://gitlab.com/vortexed.profesional) | Updated
| **RU** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | [Infernales](https://gitlab.com/Infernales) | Updated
| **FR** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | Eliha#4918 | Updated

# _Supported languages ​​outdated_


| Translate | Percentage | Contributors | Status 
| :----------- | :---------- | :---------- | :----------
| **DE** | [![Translate %](https://img.shields.io/badge/percentage-80%25-red.svg)](#) |  ilias2143#5804 and [Mordoth]() | Outdated
| **PTbr** | [![Translate %](https://img.shields.io/badge/percentage-80%25-red.svg)](#) | [XáXá](https://gitlab.com/gbmv33) | Outdated



# _New Supported language 1.0.7_

| Translate | Percentage | Contributors | Status 
| :----------- | :---------- | :---------- | :----------
| **EN** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | WoW CMS Team | Updated
| **ES** | [![Translate %](https://img.shields.io/badge/percentage-100%25-green.svg)](#) | WoW CMS Team | Updated

# _Unsupported languages ​​outdated_


| Translate | Percentage | Contributors | Status 
| :----------- | :---------- | :---------- | :----------
| **DE** | [![Translate %](https://img.shields.io/badge/percentage-0%25-red.svg)](#) |   | N/A
| **PTbr** | [![Translate %](https://img.shields.io/badge/percentage-0%25-red.svg)](#) |  | N/A
| **NO** | [![Translate %](https://img.shields.io/badge/percentage-0%25-red.svg)](#) |   | N/A
| **SWE** | [![Translate %](https://img.shields.io/badge/percentage-0%25-red.svg)](#) |   | N/A
| **RU** | [![Translate %](https://img.shields.io/badge/percentage-0%25-red.svg)](#) |   | N/A
| **FR** | [![Translate %](https://img.shields.io/badge/percentage-0%25-red.svg)](#) |   | N/A


## Copyright

Copyright © 2019 [WoW-CMS](https://wow-cms.com).
